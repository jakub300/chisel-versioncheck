var fs = require('fs');
var ejs = require('ejs');

var options = {
    name: "PhantomFlexboxTest",
    author: "JB",
    nameSlug: "test-1",
    nameCamel: "Test1",
    features: {
        has_babel: true,
        has_jquery: true
    }
}

var file = fs.readFileSync('generator-chisel/generators/app/templates/package.json', 'utf8');
var generatedFile = ejs.render(file, options);
fs.writeFileSync('project/package.json', generatedFile);